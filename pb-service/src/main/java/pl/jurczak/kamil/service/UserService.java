package pl.jurczak.kamil.service;

import lombok.AccessLevel;
import lombok.NoArgsConstructor;
import pl.jurczak.kamil.dao.UserDAO;
import pl.jurczak.kamil.model.User;

@NoArgsConstructor(access = AccessLevel.NONE)
public class UserService {

	public static void addUser(User user) {
		UserDAO.addUser(user);
	}
}
