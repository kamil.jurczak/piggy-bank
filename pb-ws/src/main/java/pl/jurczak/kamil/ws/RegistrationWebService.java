package pl.jurczak.kamil.ws;

import pl.jurczak.kamil.model.User;
import pl.jurczak.kamil.service.UserService;

import javax.ws.rs.FormParam;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.core.Response;

@Path("/user")
public class RegistrationWebService {

	@POST
	@Path("register")
	public Response registerUser(@FormParam("username") String username,
								 @FormParam("password") String password) {
		final var user = new User(username, password);
		UserService.addUser(user);
		return Response.ok("Successfully created user '" + username + "'").build();
	}
}
