package pl.jurczak.kamil.dao.exception;

public class UnresolvedDatabaseAuthPropertiesException extends RuntimeException {

	public UnresolvedDatabaseAuthPropertiesException(final String message) {
		super(message);
	}
}
