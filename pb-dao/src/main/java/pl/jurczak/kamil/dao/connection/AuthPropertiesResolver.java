package pl.jurczak.kamil.dao.connection;

import pl.jurczak.kamil.dao.exception.UnresolvedDatabaseAuthPropertiesException;

import java.io.IOException;
import java.util.Properties;

import static java.util.Objects.isNull;

public class AuthPropertiesResolver {

	private AuthPropertiesResolver() {
	}

	public static Properties getSuperUserProperties() {
		final var loader = Thread.currentThread().getContextClassLoader();
		final var stream = loader.getResourceAsStream("hibernateAuth.properties");
		if (isNull(stream)) {
			throw new UnresolvedDatabaseAuthPropertiesException("File 'hibernateAuth.properties' not exists.");
		}
		final var properties = new Properties();
		try {
			properties.load(stream);
		} catch (IOException e) {
			throw new UnresolvedDatabaseAuthPropertiesException("Cannot read 'hibernateAuth.properties' file");
		}
		return properties;
	}

	public static void validateProperties() {
		final Properties properties = getSuperUserProperties();
		final var user = properties.getProperty("db.user");
		final var password = properties.getProperty("db.password");
		if (isNull(user)) {
			throw new UnresolvedDatabaseAuthPropertiesException("Cannot find 'db.user' property in 'hibernateAuth.properties'");
		} else if (isNull(password)) {
			throw new UnresolvedDatabaseAuthPropertiesException("Cannot find 'db.password' property in 'hibernateAuth.properties");
		}
	}
}
