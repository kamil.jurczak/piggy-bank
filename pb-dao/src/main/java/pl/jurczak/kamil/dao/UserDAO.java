package pl.jurczak.kamil.dao;

import lombok.AccessLevel;
import lombok.NoArgsConstructor;
import pl.jurczak.kamil.dao.util.DaoHelper;
import pl.jurczak.kamil.model.User;

@NoArgsConstructor(access = AccessLevel.NONE)
public class UserDAO {

	public static void addUser(User user) {
		DaoHelper.saveRecord(user);
	}
}
