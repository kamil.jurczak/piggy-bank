package pl.jurczak.kamil.dao.util;

import lombok.AccessLevel;
import lombok.NoArgsConstructor;
import org.hibernate.Transaction;
import pl.jurczak.kamil.dao.connection.HibernateUtil;
import pl.jurczak.kamil.model.BaseEntity;

import static java.util.Objects.nonNull;

@NoArgsConstructor(access = AccessLevel.NONE)
public class DaoHelper {

	public static boolean saveRecord(BaseEntity entity) {
		var success = false;
		Transaction transaction = null;
		try (var session = HibernateUtil.getSessionFactory().openSession()) {
			transaction = session.beginTransaction();
			session.save(entity);
			transaction.commit();
			success = true;
		} catch (Exception e) {
			if (nonNull(transaction)) {
				transaction.rollback();
			}
			e.printStackTrace();
		}
		return success;
	}
}
