package pl.jurczak.kamil.dao.connection;

import lombok.AccessLevel;
import lombok.NoArgsConstructor;
import org.hibernate.SessionFactory;
import org.hibernate.boot.registry.StandardServiceRegistryBuilder;
import org.hibernate.cfg.Configuration;
import pl.jurczak.kamil.model.User;
import pl.jurczak.kamil.model.UserApplicationProperties;

import java.util.Properties;

import static java.util.Objects.isNull;
import static org.hibernate.cfg.Environment.*;

@NoArgsConstructor(access = AccessLevel.NONE)
public class HibernateUtil {

	private static SessionFactory sessionFactory = getSessionFactory();

	public static SessionFactory getSessionFactory() {
		if (isNull(sessionFactory)) {
			try {
				final var settings = new Properties();
				settings.put(DRIVER, "org.postgresql.Driver");
				settings.put(URL, "jdbc:postgresql://localhost:5432/piggy_bank_dev?useSSL=false");

				final var authProperties = AuthPropertiesResolver.getSuperUserProperties();
				settings.put(USER, authProperties.getProperty("db.user"));
				settings.put(PASS, authProperties.getProperty("db.password"));

				settings.put(DIALECT, "org.hibernate.dialect.PostgreSQL95Dialect");
				settings.put(SHOW_SQL, "true");
				settings.put(HBM2DDL_AUTO, "create-drop");

				final var configuration = new Configuration();
				configuration.setProperties(settings);
				configuration.addAnnotatedClass(User.class);
				configuration.addAnnotatedClass(UserApplicationProperties.class);

				final var serviceRegistry = new StandardServiceRegistryBuilder()
						.applySettings(configuration.getProperties()).build();

				sessionFactory = configuration.buildSessionFactory(serviceRegistry);
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
		return sessionFactory;
	}
}
