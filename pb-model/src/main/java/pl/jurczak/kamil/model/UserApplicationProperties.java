package pl.jurczak.kamil.model;

import lombok.NoArgsConstructor;

import javax.persistence.Entity;
import java.io.Serializable;

@Entity(name = "user_application_properties")
@NoArgsConstructor
public class UserApplicationProperties extends BaseEntity implements Serializable {

	private String properties;
}
