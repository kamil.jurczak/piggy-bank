package pl.jurczak.kamil.model;

import lombok.NoArgsConstructor;

import javax.persistence.Entity;
import java.io.Serializable;

@Entity(name = "users")
@NoArgsConstructor
public class User extends BaseEntity implements Serializable {

	public User(String username, String password) {
		this.username = username;
		this.password = password;
	}

	private String username;
	private String password;
	private String firstName;
	private String lastName;
	private String email;
	private String phone;
}
